# Contributor: Patrick Jackson <PatrickSJackson gmail com>
# Maintainer: Christoph Vigano <mail@cvigano.de>

pkgname=st
pkgver=0.8.1
pkgrel=1
pkgdesc='A simple virtual terminal emulator for X.'
arch=('i686' 'x86_64')
license=('MIT')
depends=('libxft' 'libxext' 'xorg-fonts-misc')
makedepends=('ncurses')
url="http://st.suckless.org"
source=(http://dl.suckless.org/st/$pkgname-$pkgver.tar.gz
        https://st.suckless.org/patches/anysize/st-anysize-20180817.diff
        https://st.suckless.org/patches/scrollback/st-scrollback-0.8.diff
        https://st.suckless.org/patches/scrollback/st-scrollback-mouse-0.8.diff
        https://st.suckless.org/patches/scrollback/st-scrollback-mouse-altscreen-0.8.diff
        config.h)
sha256sums=('c4fb0fe2b8d2d3bd5e72763e80a8ae05b7d44dbac8f8e3bb18ef0161c7266926'
            'be6a5c9033f8d721767b051842015c03df5fa22487d6ebced9b4e20d68e8d1b5'
            '8279d347c70bc9b36f450ba15e1fd9ff62eedf49ce9258c35d7f1cfe38cca226'
            '3fb38940cc3bad3f9cd1e2a0796ebd0e48950a07860ecf8523a5afd0cd1b5a44'
            'e9f73c4de379d54ae107d0f4dd7b068a6628326c1bd7e11bfcd33d9079290347'
            'ab40459c80328f4602802952772f97a4e5b381e230561dc4c5b1a5328fb44307')

prepare() {
  cd $srcdir/$pkgname-$pkgver
  # skip terminfo which conflicts with nsurses
  sed -i '/tic /d' Makefile
  cp $srcdir/config.h config.h

  patch -Np1 -i $srcdir/st-anysize-20180817.diff
  patch -Np1 -i $srcdir/st-scrollback-0.8.diff
  patch -Np1 -i $srcdir/st-scrollback-mouse-0.8.diff
  patch -Np1 -i $srcdir/st-scrollback-mouse-altscreen-0.8.diff
}

build() {
  cd $srcdir/$pkgname-$pkgver
  make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
  cd $srcdir/$pkgname-$pkgver
  make PREFIX=/usr DESTDIR="$pkgdir" TERMINFO="$pkgdir/usr/share/terminfo" install
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 README "$pkgdir/usr/share/doc/$pkgname/README"
}
